# https://docs.python.org/3/library/unittest.html
import unittest
from nmea import nmea_message, input_stream, util

# run tests from project root: python3 -m unittest test.coordinate_test  

class TestNMEA(unittest.TestCase):
    def setUp(self) -> None:
        return super().setUp()

    def test_coord_fron_string_1(self):
        line = b"$GNGGA,185724.000,3102.2749,N,00941.9214,E,1,04,1.8,275.1,M,50.5,M,,0000*49"
        msg = nmea_message.NMEAMessage(line)
        msg_tokens = nmea_message.NMEAMessage.break_message(msg)

        lat = util.coord_from_string(msg_tokens[2], msg_tokens[3])
        lon = util.coord_from_string(msg_tokens[4], msg_tokens[5])
        self.assertEqual(lat, 31.037915)
        self.assertEqual(lon, 9.69869)

    def test_coord_fron_string_2(self):
        line = b"$GNGGA,012850.00,4539.69200,N,10848.26323,W,2,12,0.55,1022.7,M,-18.3,M,,0000*4A"
        msg = nmea_message.NMEAMessage(line)
        msg_tokens = nmea_message.NMEAMessage.break_message(msg)

        lat = util.coord_from_string(msg_tokens[2], msg_tokens[3])
        lon = util.coord_from_string(msg_tokens[4], msg_tokens[5])
        self.assertEqual(lat, 45.66153333333333)
        self.assertEqual(lon, -108.80438716666667 )

    def test_types_west(self):
        line = b"$GNGLL,4539.69200,N,10848.26323,W,012850.00,A,D*6B"
        msg = nmea_message.NMEAMessage.load_message(line)
        self.assertEqual(msg.latitude, 45.66153333333333)
        self.assertEqual(msg.longitude, -108.80438716666667)

        line = b"$GNGGA,012851.00,4539.68555,N,10848.28542,W,2,12,0.55,1022.9,M,-18.3,M,,0000*4C"
        msg = nmea_message.NMEAMessage.load_message(line)
        self.assertEqual(msg.latitude, 45.66142583333333)
        self.assertEqual(msg.longitude, -108.804757)

        line = b"$GNGLL,4539.68555,N,10848.28542,W,012851.00,A,D*63"
        msg = nmea_message.NMEAMessage.load_message(line)
        self.assertEqual(msg.latitude, 45.66142583333333)
        self.assertEqual(msg.longitude, -108.804757)

        line = b"$GNRMC,012852.00,A,4539.67922,N,10848.30768,W,60.638,248.13,210621,,,D,V*2C"
        msg = nmea_message.NMEAMessage.load_message(line)
        self.assertEqual(msg.latitude, 45.661320333333336)
        self.assertEqual(msg.longitude, -108.805128)

        line = b"$GNGGA,012852.00,4539.67922,N,10848.30768,W,2,12,0.55,1023.1,M,-18.3,M,,0000*46"
        msg = nmea_message.NMEAMessage.load_message(line)
        self.assertEqual(msg.latitude, 45.661320333333336)
        self.assertEqual(msg.longitude, -108.805128)

    def test_types_east(self):
        line = b"$GNGGA,185724.000,3102.2749,N,00941.9214,E,1,04,1.8,275.1,M,50.5,M,,0000*49"
        msg = nmea_message.NMEAMessage.load_message(line)
        self.assertEqual(msg.latitude, 31.037915)
        self.assertEqual(msg.longitude, 9.69869)

        line = b"$GNGLL,3102.2749,N,00941.9214,E,012851.00,A,D*77"
        msg = nmea_message.NMEAMessage.load_message(line)
        self.assertEqual(msg.latitude, 31.037915)
        self.assertEqual(msg.longitude, 9.69869)

        line = b"$GNRMC,012852.00,A,3102.2749,N,00941.9214,E,60.638,248.13,210621,,,D,V*3B"
        msg = nmea_message.NMEAMessage.load_message(line)
        self.assertEqual(msg.latitude, 31.037915)
        self.assertEqual(msg.longitude, 9.69869)

    def test_sample(self):
        stream = input_stream.GenericInputStream.open_stream('sample-data/sample1.txt')

        with stream:
            line = stream.get_line()
            msg = nmea_message.NMEAMessage.load_message(line)
            self.assertEqual(msg.latitude, 45.66153333333333)
            self.assertEqual(msg.longitude, -108.80438716666667)

            line = stream.get_line()
            line = stream.get_line()

            msg = nmea_message.NMEAMessage.load_message(line)
            self.assertEqual(msg.latitude, 45.66153333333333)
            self.assertEqual(msg.longitude, -108.80438716666667)

if __name__ == '__main__':
    unittest.main()